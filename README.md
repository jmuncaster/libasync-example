# LibAsync Example

Example of using a thread pool to do asyncronous processing in a main program loop.

## Description

This project shows how one might use this [threading library](https://github.com/florischabert/libasync) to implement an
image processing program loop.

## Getting Started

### Dependencies

* [cmake](https://www.cmake.org)

### Build and Run

To build the program, run the following:

```bash
mkdir build && cd build
cmake ..
make
./thread_pool_example
```

## Authors

Justin Muncaster

## License

This project is licensed under the MIT License - see the LICENSE.md file for details

## Acknowledgments

* [libasync](https://github.com/florischabert/libasync)

